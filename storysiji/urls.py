from django.urls import path
from storysiji import views

urlpatterns = [
    path('', views.index, name='index'),
]
